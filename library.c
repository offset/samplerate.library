/*
   samplerate.library, library skeleton
   Generated with LibMaker 0.11.
*/

/****** samplerate.library/background ***************************************
*
* DESCRIPTION
*
* HISTORY
*
*****************************************************************************
*
*/

#define __NOLIBBASE__

#include <proto/exec.h>
#include <exec/resident.h>
#include <exec/libraries.h>
#include <libraries/query.h>
#include <clib/alib_protos.h>
#include <clib/debug_protos.h>

#define UNUSED __attribute__((unused))

#include "lib_version.h"
#include "library.h"

#include "lib/samplerate.h"

const char LibName[] = LIBNAME;
extern const char VTag[];

struct Library *SysBase;


struct Library *LibInit(struct Library *unused, APTR seglist, struct Library *sysb);
struct MyLibBase *lib_init(struct MyLibBase *base, APTR seglist, struct Library *SysBase);
APTR lib_expunge(struct MyLibBase *base);
struct Library *LibOpen(void);
ULONG LibClose(void);
APTR LibExpunge(void);
ULONG LibReserved(void);
BOOL InitResources(struct MyLibBase *base);
VOID FreeResources(struct MyLibBase *base);

SRC_STATE * Libsrc_new(struct MyLibBase *base, int converter_type, int channels, int *error);
SRC_STATE * Libsrc_clone(struct MyLibBase *base, SRC_STATE *orig, int *error);
SRC_STATE * Libsrc_callback_new(struct MyLibBase *base, src_callback_t func, int converter_type, int channels, int *error, void *cb_data);
SRC_STATE * Libsrc_delete(struct MyLibBase *base, SRC_STATE *state);
int Libsrc_process(struct MyLibBase *base, SRC_STATE *state, SRC_DATA *data);
long Libsrc_callback_read(struct MyLibBase *base, SRC_STATE *state, double src_ratio, long frames, float *data);
int Libsrc_simple(struct MyLibBase *base, SRC_DATA *data, int converter_type, int channels);
const char * Libsrc_get_name(struct MyLibBase *base, int converter_type);
const char * Libsrc_get_description(struct MyLibBase *base, int converter_type);
const char * Libsrc_get_version(struct MyLibBase *base);
int Libsrc_set_ratio(struct MyLibBase *base, SRC_STATE *state, double new_ratio);
int Libsrc_get_channels(struct MyLibBase *base, SRC_STATE *state);
int Libsrc_reset(struct MyLibBase *base, SRC_STATE *state);
int Libsrc_is_valid_ratio(struct MyLibBase *base, double ratio);
int Libsrc_error(struct MyLibBase *base, SRC_STATE *state);
const char * Libsrc_strerror(struct MyLibBase *base, int error);
void Libsrc_short_to_float_array(struct MyLibBase *base, const short *in, float *out, int len);
void Libsrc_float_to_short_array(struct MyLibBase *base, const float *in, short *out, int len);
void Libsrc_int_to_float_array(struct MyLibBase *base, const int *in, float *out, int len);
void Libsrc_float_to_int_array(struct MyLibBase *base, const float *in, int *out, int len);



BOOL InitResources(UNUSED struct MyLibBase *base)
{
	return TRUE;
}


VOID FreeResources(UNUSED struct MyLibBase *base)
{
}


const struct TagItem RTags[] =
{
	{ QUERYINFOATTR_NAME, (IPTR)LibName },
	{ QUERYINFOATTR_IDSTRING, (IPTR)&VTag[1] },
	{ QUERYINFOATTR_COPYRIGHT, (IPTR)"� 2002-2016, Erik de Castro Lopo <erikd@mega-nerd.com>" },
	{ QUERYINFOATTR_DATE, (IPTR)DATE },
	{ QUERYINFOATTR_VERSION, VERSION },
	{ QUERYINFOATTR_REVISION, REVISION },
	{ QUERYINFOATTR_SUBTYPE, QUERYSUBTYPE_LIBRARY },
	{ TAG_END,  0 }
};

struct Resident ROMTag =
{
	RTC_MATCHWORD,
	&ROMTag,
	&ROMTag + 1,
	RTF_EXTENDED | RTF_PPC,
	VERSION,
	NT_LIBRARY,
	0,
	(char*)LibName,
	VSTRING,
	(APTR)LibInit,
	REVISION,
	(struct TagItem*)RTags
};


APTR JumpTable[] =
{
	(APTR)FUNCARRAY_BEGIN,
	(APTR)FUNCARRAY_32BIT_NATIVE,
	(APTR)LibOpen,
	(APTR)LibClose,
	(APTR)LibExpunge,
	(APTR)LibReserved,
	(APTR)0xFFFFFFFF,
	(APTR)FUNCARRAY_32BIT_SYSTEMV,
	(APTR)Libsrc_new,
	(APTR)Libsrc_clone,
	(APTR)Libsrc_callback_new,
	(APTR)Libsrc_delete,
	(APTR)Libsrc_process,
	(APTR)Libsrc_callback_read,
	(APTR)Libsrc_simple,
	(APTR)Libsrc_get_name,
	(APTR)Libsrc_get_description,
	(APTR)Libsrc_get_version,
	(APTR)Libsrc_set_ratio,
	(APTR)Libsrc_get_channels,
	(APTR)Libsrc_reset,
	(APTR)Libsrc_is_valid_ratio,
	(APTR)Libsrc_error,
	(APTR)Libsrc_strerror,
	(APTR)Libsrc_short_to_float_array,
	(APTR)Libsrc_float_to_short_array,
	(APTR)Libsrc_int_to_float_array,
	(APTR)Libsrc_float_to_int_array,
	(APTR)0xFFFFFFFF,
	(APTR)FUNCARRAY_END
};


struct MyLibBase * lib_init(struct MyLibBase *base, APTR seglist, UNUSED struct Library *sysbase)
{
	InitSemaphore(&base->BaseLock);
	base->Seglist = seglist;
	return base;
}

struct TagItem LibTags[] = {
	{ LIBTAG_FUNCTIONINIT, (IPTR)JumpTable },
	{ LIBTAG_LIBRARYINIT,  (IPTR)lib_init },
	{ LIBTAG_MACHINE,      MACHINE_PPC },
	{ LIBTAG_BASESIZE,     sizeof(struct MyLibBase) },
	{ LIBTAG_SEGLIST,      0 },
	{ LIBTAG_TYPE,         NT_LIBRARY },
	{ LIBTAG_NAME,         0 },
	{ LIBTAG_IDSTRING,     0 },
	{ LIBTAG_FLAGS,        LIBF_CHANGED | LIBF_SUMUSED },
	{ LIBTAG_VERSION,      VERSION },
	{ LIBTAG_REVISION,     REVISION },
	{ LIBTAG_PUBLIC,       TRUE },
	{ TAG_END,             0 },
};

struct Library * LibInit(UNUSED struct Library *unused, APTR seglist, struct Library *sysbase)
{
	SysBase = sysbase;

	LibTags[4].ti_Data = (IPTR)seglist;
	LibTags[6].ti_Data = (IPTR)ROMTag.rt_Name;
	LibTags[7].ti_Data = (IPTR)ROMTag.rt_IdString;

	return (NewCreateLibrary(LibTags));
}


struct Library * LibOpen(void)
{
	struct MyLibBase *base = (struct MyLibBase*)REG_A6;
	struct Library *lib = (struct Library*)base;

	ObtainSemaphore(&base->BaseLock);

	if (!base->InitFlag)
	{
		if (InitResources(base)) base->InitFlag = TRUE;
		else
		{
			FreeResources(base);
			lib = NULL;
		}
	}

	if (lib)
	{
		base->LibNode.lib_Flags &= ~LIBF_DELEXP;
		base->LibNode.lib_OpenCnt++;
	}

	ReleaseSemaphore(&base->BaseLock);
	if (!lib) lib_expunge(base);
	return lib;
}


ULONG LibClose(void)
{
	struct MyLibBase *base = (struct MyLibBase*)REG_A6;
	ULONG ret = 0;

	ObtainSemaphore(&base->BaseLock);

	if (--base->LibNode.lib_OpenCnt == 0)
	{
		if (base->LibNode.lib_Flags & LIBF_DELEXP) ret = (ULONG)lib_expunge(base);
	}

	if (ret == 0) ReleaseSemaphore(&base->BaseLock);
	return ret;
}


APTR LibExpunge(void)
{
	struct MyLibBase *base = (struct MyLibBase*)REG_A6;

	return(lib_expunge(base));
}


APTR lib_expunge(struct MyLibBase *base)
{
	APTR seglist = NULL;

	ObtainSemaphore(&base->BaseLock);

	if (base->LibNode.lib_OpenCnt == 0)
	{
		FreeResources(base);
		Forbid();
		Remove((struct Node*)base);
		Permit();
		seglist = base->Seglist;
		FreeMem((UBYTE*)base - base->LibNode.lib_NegSize, base->LibNode.lib_NegSize + base->LibNode.lib_PosSize);
		base = NULL;    /* freed memory, no more valid */
	}
	else base->LibNode.lib_Flags |= LIBF_DELEXP;

	if (base) ReleaseSemaphore(&base->BaseLock);
	return seglist;
}


ULONG LibReserved(void)
{
	return 0;
}

/****** samplerate.library/src_new ******************************************
*
* NAME
*   src_new -- {short} (V1)
*
* SYNOPSIS
*   SRC_STATE * src_new(int converter_type, int channels, int *error)
*
* FUNCTION
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

SRC_STATE * Libsrc_new(UNUSED struct MyLibBase *base, int converter_type, int channels, int *error)
{
    return src_new(converter_type, channels, error);
}



/****** samplerate.library/src_clone ****************************************
*
* NAME
*   src_clone -- {short} (V1)
*
* SYNOPSIS
*   SRC_STATE * src_clone(SRC_STATE *orig, int *error)
*
* FUNCTION
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

SRC_STATE * Libsrc_clone(UNUSED struct MyLibBase *base, SRC_STATE *orig, int *error)
{
    return src_clone(orig, error);
}



/****** samplerate.library/src_callback_new *********************************
*
* NAME
*   src_callback_new -- {short} (V1)
*
* SYNOPSIS
*   SRC_STATE * src_callback_new(src_callback_t func, int converter_type, int channels, int *error, void *cb_data)
*
* FUNCTION
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

SRC_STATE * Libsrc_callback_new(UNUSED struct MyLibBase *base, src_callback_t func, int converter_type, int channels, int *error, void *cb_data)
{
    return src_callback_new(func, converter_type, channels, error, cb_data);
}



/****** samplerate.library/src_delete ***************************************
*
* NAME
*   src_delete -- {short} (V1)
*
* SYNOPSIS
*   SRC_STATE * src_delete(SRC_STATE *state)
*
* FUNCTION
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

SRC_STATE * Libsrc_delete(UNUSED struct MyLibBase *base, SRC_STATE *state)
{
    return src_delete(state);
}



/****** samplerate.library/src_process **************************************
*
* NAME
*   src_process -- {short} (V1)
*
* SYNOPSIS
*   int src_process(SRC_STATE *state, SRC_DATA *data)
*
* FUNCTION
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

int Libsrc_process(UNUSED struct MyLibBase *base, SRC_STATE *state, SRC_DATA *data)
{
    return src_process(state, data);
}



/****** samplerate.library/src_callback_read ********************************
*
* NAME
*   src_callback_read -- {short} (V1)
*
* SYNOPSIS
*   long src_callback_read(SRC_STATE *state, double src_ratio, long frames, float *data)
*
* FUNCTION
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

long Libsrc_callback_read(UNUSED struct MyLibBase *base, SRC_STATE *state, double src_ratio, long frames, float *data)
{
    return src_callback_read(state, src_ratio, frames, data);
}



/****** samplerate.library/src_simple ***************************************
*
* NAME
*   src_simple -- {short} (V1)
*
* SYNOPSIS
*   int src_simple(SRC_DATA *data, int converter_type, int channels)
*
* FUNCTION
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

int Libsrc_simple(UNUSED struct MyLibBase *base, SRC_DATA *data, int converter_type, int channels)
{
    return src_simple(data, converter_type, channels);
}



/****** samplerate.library/src_get_name *************************************
*
* NAME
*   src_get_name -- {short} (V1)
*
* SYNOPSIS
*   const char * src_get_name(int converter_type)
*
* FUNCTION
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

const char * Libsrc_get_name(UNUSED struct MyLibBase *base, int converter_type)
{
    return src_get_name(converter_type);
}



/****** samplerate.library/src_get_description ******************************
*
* NAME
*   src_get_description -- {short} (V1)
*
* SYNOPSIS
*   const char * src_get_description(int converter_type)
*
* FUNCTION
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

const char * Libsrc_get_description(UNUSED struct MyLibBase *base, int converter_type)
{
    return src_get_description(converter_type);
}



/****** samplerate.library/src_get_version **********************************
*
* NAME
*   src_get_version -- {short} (V1)
*
* SYNOPSIS
*   const char * src_get_version(VOID)
*
* FUNCTION
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

const char * Libsrc_get_version(UNUSED struct MyLibBase *base)
{
    return src_get_version();
}



/****** samplerate.library/src_set_ratio ************************************
*
* NAME
*   src_set_ratio -- {short} (V1)
*
* SYNOPSIS
*   int src_set_ratio(SRC_STATE *state, double new_ratio)
*
* FUNCTION
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

int Libsrc_set_ratio(UNUSED struct MyLibBase *base, SRC_STATE *state, double new_ratio)
{
    return src_set_ratio(state, new_ratio);
}



/****** samplerate.library/src_get_channels *********************************
*
* NAME
*   src_get_channels -- {short} (V1)
*
* SYNOPSIS
*   int src_get_channels(SRC_STATE *state)
*
* FUNCTION
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

int Libsrc_get_channels(UNUSED struct MyLibBase *base, SRC_STATE *state)
{
    return src_get_channels(state);
}



/****** samplerate.library/src_reset ****************************************
*
* NAME
*   src_reset -- {short} (V1)
*
* SYNOPSIS
*   int src_reset(SRC_STATE *state)
*
* FUNCTION
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

int Libsrc_reset(UNUSED struct MyLibBase *base, SRC_STATE *state)
{
    return src_reset(state);
}



/****** samplerate.library/src_is_valid_ratio *******************************
*
* NAME
*   src_is_valid_ratio -- {short} (V1)
*
* SYNOPSIS
*   int src_is_valid_ratio(double ratio)
*
* FUNCTION
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

int Libsrc_is_valid_ratio(UNUSED struct MyLibBase *base, double ratio)
{
    return src_is_valid_ratio(ratio);
}



/****** samplerate.library/src_error ****************************************
*
* NAME
*   src_error -- {short} (V1)
*
* SYNOPSIS
*   int src_error(SRC_STATE *state)
*
* FUNCTION
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

int Libsrc_error(UNUSED struct MyLibBase *base, SRC_STATE *state)
{
    return src_error(state);
}



/****** samplerate.library/src_strerror *************************************
*
* NAME
*   src_strerror -- {short} (V1)
*
* SYNOPSIS
*   const char * src_strerror(int error)
*
* FUNCTION
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

const char * Libsrc_strerror(UNUSED struct MyLibBase *base, int error)
{
    return src_strerror(error);
}



/****** samplerate.library/src_short_to_float_array *************************
*
* NAME
*   src_short_to_float_array -- {short} (V1)
*
* SYNOPSIS
*   void src_short_to_float_array(const short *in, float *out, int len)
*
* FUNCTION
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

void Libsrc_short_to_float_array(UNUSED struct MyLibBase *base, const short *in, float *out, int len)
{
    src_short_to_float_array(in, out, len);
}



/****** samplerate.library/src_float_to_short_array *************************
*
* NAME
*   src_float_to_short_array -- {short} (V1)
*
* SYNOPSIS
*   void src_float_to_short_array(const float *in, short *out, int len)
*
* FUNCTION
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

void Libsrc_float_to_short_array(UNUSED struct MyLibBase *base, const float *in, short *out, int len)
{
    src_float_to_short_array(in, out, len);
}



/****** samplerate.library/src_int_to_float_array ***************************
*
* NAME
*   src_int_to_float_array -- {short} (V1)
*
* SYNOPSIS
*   void src_int_to_float_array(const int *in, float *out, int len)
*
* FUNCTION
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

void Libsrc_int_to_float_array(UNUSED struct MyLibBase *base, const int *in, float *out, int len)
{
    return src_int_to_float_array(in, out, len);
}



/****** samplerate.library/src_float_to_int_array ***************************
*
* NAME
*   src_float_to_int_array -- {short} (V1)
*
* SYNOPSIS
*   void src_float_to_int_array(const float *in, int *out, int len)
*
* FUNCTION
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

void Libsrc_float_to_int_array(UNUSED struct MyLibBase *base, const float* in, int* out, int len)
{
    return src_float_to_int_array(in, out, len);
}


