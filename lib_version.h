/*
   samplerate.library version header file.
   Generated with LibMaker 0.11.
*/

#ifndef SAMPLERATE_LIB_VERSION_H
#define SAMPLERATE_LIB_VERSION_H

#define LIBNAME "samplerate.library"
#define VERSION 1
#define REVISION 0
#define DATE "03.07.2021"

#define COPYRIGHT "� 2002-2016, Erik de Castro Lopo, MorphOS port � 2021 Philippe Rimauro"

#define VSTRING LIBNAME " " XSTR(VERSION) "." XSTR(REVISION) " (" DATE ") " COPYRIGHT
#define VERSTAG "\0$VER: " VSTRING

#define XSTR(s) STR(s)
#define STR(s) #s

#endif      /* SAMPLERATE_LIB_VERSION_H */
