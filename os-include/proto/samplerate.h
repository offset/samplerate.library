/* Automatically generated header! Do not edit! */

#ifndef PROTO_SAMPLERATE_H
#define PROTO_SAMPLERATE_H

#ifndef __NOLIBBASE__
extern struct Library *
#ifdef __CONSTLIBBASEDECL__
__CONSTLIBBASEDECL__
#endif /* __CONSTLIBBASEDECL__ */
SampleRateBase;
#endif /* !__NOLIBBASE__ */

#include <clib/samplerate_protos.h>

#ifdef __GNUC__
#ifdef __PPC__
#ifndef _NO_PPCINLINE
#include <ppcinline/samplerate.h>
#endif /* _NO_PPCINLINE */
#else
#ifndef _NO_INLINE
#include <inline/samplerate.h>
#endif /* _NO_INLINE */
#endif /* __PPC__ */
#elif defined(__VBCC__)
#include <inline/samplerate_protos.h>
#else
#include <pragmas/samplerate_pragmas.h>
#endif /* __GNUC__ */

#endif /* !PROTO_SAMPLERATE_H */

