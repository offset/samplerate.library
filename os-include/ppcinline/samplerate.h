/* Automatically generated header! Do not edit! */

#ifndef _PPCINLINE_SAMPLERATE_H
#define _PPCINLINE_SAMPLERATE_H

#ifndef __PPCINLINE_MACROS_H
#include <ppcinline/macros.h>
#endif /* !__PPCINLINE_MACROS_H */

#ifndef SAMPLERATE_BASE_NAME
#define SAMPLERATE_BASE_NAME SampleRateBase
#endif /* !SAMPLERATE_BASE_NAME */

#define src_new(__p0, __p1, __p2) \
	(((SRC_STATE *(*)(void *, int , int , int *))*(void**)((long)(SAMPLERATE_BASE_NAME) - 28))((void*)(SAMPLERATE_BASE_NAME), __p0, __p1, __p2))

#define src_clone(__p0, __p1) \
	(((SRC_STATE *(*)(void *, SRC_STATE *, int *))*(void**)((long)(SAMPLERATE_BASE_NAME) - 34))((void*)(SAMPLERATE_BASE_NAME), __p0, __p1))

#define src_callback_new(__p0, __p1, __p2, __p3, __p4) \
	(((SRC_STATE *(*)(void *, src_callback_t , int , int , int *, void *))*(void**)((long)(SAMPLERATE_BASE_NAME) - 40))((void*)(SAMPLERATE_BASE_NAME), __p0, __p1, __p2, __p3, __p4))

#define src_delete(__p0) \
	(((SRC_STATE *(*)(void *, SRC_STATE *))*(void**)((long)(SAMPLERATE_BASE_NAME) - 46))((void*)(SAMPLERATE_BASE_NAME), __p0))

#define src_process(__p0, __p1) \
	(((int (*)(void *, SRC_STATE *, SRC_DATA *))*(void**)((long)(SAMPLERATE_BASE_NAME) - 52))((void*)(SAMPLERATE_BASE_NAME), __p0, __p1))

#define src_callback_read(__p0, __p1, __p2, __p3) \
	(((long (*)(void *, SRC_STATE *, double , long , float *))*(void**)((long)(SAMPLERATE_BASE_NAME) - 58))((void*)(SAMPLERATE_BASE_NAME), __p0, __p1, __p2, __p3))

#define src_simple(__p0, __p1, __p2) \
	(((int (*)(void *, SRC_DATA *, int , int ))*(void**)((long)(SAMPLERATE_BASE_NAME) - 64))((void*)(SAMPLERATE_BASE_NAME), __p0, __p1, __p2))

#define src_get_name(__p0) \
	(((const char *(*)(void *, int ))*(void**)((long)(SAMPLERATE_BASE_NAME) - 70))((void*)(SAMPLERATE_BASE_NAME), __p0))

#define src_get_description(__p0) \
	(((const char *(*)(void *, int ))*(void**)((long)(SAMPLERATE_BASE_NAME) - 76))((void*)(SAMPLERATE_BASE_NAME), __p0))

#define src_get_version() \
	(((const char *(*)(void *))*(void**)((long)(SAMPLERATE_BASE_NAME) - 82))((void*)(SAMPLERATE_BASE_NAME)))

#define src_set_ratio(__p0, __p1) \
	(((int (*)(void *, SRC_STATE *, double ))*(void**)((long)(SAMPLERATE_BASE_NAME) - 88))((void*)(SAMPLERATE_BASE_NAME), __p0, __p1))

#define src_get_channels(__p0) \
	(((int (*)(void *, SRC_STATE *))*(void**)((long)(SAMPLERATE_BASE_NAME) - 94))((void*)(SAMPLERATE_BASE_NAME), __p0))

#define src_reset(__p0) \
	(((int (*)(void *, SRC_STATE *))*(void**)((long)(SAMPLERATE_BASE_NAME) - 100))((void*)(SAMPLERATE_BASE_NAME), __p0))

#define src_is_valid_ratio(__p0) \
	(((int (*)(void *, double ))*(void**)((long)(SAMPLERATE_BASE_NAME) - 106))((void*)(SAMPLERATE_BASE_NAME), __p0))

#define src_error(__p0) \
	(((int (*)(void *, SRC_STATE *))*(void**)((long)(SAMPLERATE_BASE_NAME) - 112))((void*)(SAMPLERATE_BASE_NAME), __p0))

#define src_strerror(__p0) \
	(((const char *(*)(void *, int ))*(void**)((long)(SAMPLERATE_BASE_NAME) - 118))((void*)(SAMPLERATE_BASE_NAME), __p0))

#define src_short_to_float_array(__p0, __p1, __p2) \
	(((void (*)(void *, const short *, float *, int ))*(void**)((long)(SAMPLERATE_BASE_NAME) - 124))((void*)(SAMPLERATE_BASE_NAME), __p0, __p1, __p2))

#define src_float_to_short_array(__p0, __p1, __p2) \
	(((void (*)(void *, const float *, short *, int ))*(void**)((long)(SAMPLERATE_BASE_NAME) - 130))((void*)(SAMPLERATE_BASE_NAME), __p0, __p1, __p2))

#define src_int_to_float_array(__p0, __p1, __p2) \
	(((void (*)(void *, const int *, float *, int ))*(void**)((long)(SAMPLERATE_BASE_NAME) - 136))((void*)(SAMPLERATE_BASE_NAME), __p0, __p1, __p2))

#define src_float_to_int_array(__p0, __p1, __p2) \
	(((void (*)(void *, const float *, int *, int ))*(void**)((long)(SAMPLERATE_BASE_NAME) - 142))((void*)(SAMPLERATE_BASE_NAME), __p0, __p1, __p2))

#endif /* !_PPCINLINE_SAMPLERATE_H */
