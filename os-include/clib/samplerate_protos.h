#ifndef CLIB_SAMPLERATE_PROTOS_H
#define CLIB_SAMPLERATE_PROTOS_H

/*
   samplerate.library C prototypes
   Copyright � � 2002-2016, Erik de Castro Lopo <erikd@mega-nerd.com>
   Generated with LibMaker 0.11.
*/


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <libraries/samplerate.h>

SRC_STATE * src_new(int, int, int *);
SRC_STATE * src_clone(SRC_STATE *, int *);
SRC_STATE * src_callback_new(src_callback_t, int, int, int *, void *);
SRC_STATE * src_delete(SRC_STATE *);
int src_process(SRC_STATE *, SRC_DATA *);
long src_callback_read(SRC_STATE *, double, long, float *);
int src_simple(SRC_DATA *, int, int);
const char * src_get_name(int);
const char * src_get_description(int);
const char * src_get_version(VOID);
int src_set_ratio(SRC_STATE *, double);
int src_get_channels(SRC_STATE *);
int src_reset(SRC_STATE *);
int src_is_valid_ratio(double);
int src_error(SRC_STATE *);
const char * src_strerror(int);
void src_short_to_float_array(const short *, float *, int);
void src_float_to_short_array(const float *, short *, int);
void src_int_to_float_array(const int *, float *, int);
void src_float_to_int_array(const float *, int *, int);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CLIB_SAMPLERATE_PROTOS_H */
