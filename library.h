/*
   samplerate.library internal library definitions.
   Generated with LibMaker 0.11.
*/

#ifndef SAMPLERATE_LIBRARY_H
#define SAMPLERATE_LIBRARY_H

#include <exec/libraries.h>
#include <exec/semaphores.h>


struct MyLibBase
{
	struct Library          LibNode;
	APTR                    Seglist;
	struct SignalSemaphore  BaseLock;
	BOOL                    InitFlag;
};

#endif      /* SAMPLERATE_LIBRARY_H */
